<?php

namespace App\Birthday;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Birthday extends DB{

    public $id="";
    public $name="";
    public $birth_date="";

    public function __construct()
    {
        parent::__construct();
    }

    /*
    public function index(){
        echo "<br>".$this->id."<br>";
        echo $this->name."<br>";
        echo $this->birthDate."<br>";

    }
    */

    public function setData($data=NULL){

        if(array_key_exists('name',$data)){
            $this->name=$data['name'];
        }

        if(array_key_exists('birth_date',$data)){
            $this->birth_date=$data['birth_date'];
        }

    }

    public function store(){
        $arrData=array($this->name, $this->birth_date);
        $sql="INSERT INTO Birthday (name,birth_date) VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if ($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");

        Utility::redirect('create.php'); // redirect korte hobe create.php te tai utility.php use korechi //

    }//end of store method

}

//$objBirthday = new Birthday();