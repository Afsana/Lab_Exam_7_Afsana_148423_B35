<?php

namespace App\Gender;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class Gender extends DB{

    public $id="";
    public $name="";
    public $gender="";

    public function __construct()
    {
        parent::__construct();
    }

    /*
    public function index(){
        echo $this->id."<br>";
        echo $this->name."<br>";
        echo $this->gender."<br>";
    }
    */

    public function setData($data=NULL){

        if(array_key_exists('name',$data)){
            $this->name=$data['name'];
        }

        if(array_key_exists('gender',$data)){
            $this->gender=$data['gender'];
        }

    }

    public function store(){
        $arrData=array($this->name, $this->gender);
        $sql="INSERT INTO gender (name,gender) VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if ($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");

        Utility::redirect('create.php'); // redirect korte hobe create.php te tai utility.php use korechi //

    }//end of store method


}


//$objGender = new Gender();