<?php

namespace App\BookTitle;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class BookTitle extends DB{

    public $id="";
    public $book_title="";
    public $author_name="";

    public function __construct()
    {
        parent::__construct();
    }
    /*
     public function index(){

        echo $this->id."<br>";
        echo $this->book_title."<br>";
        echo $this->author_name."<br>";

    }
    */

    public function setData($data=NULL){

        if(array_key_exists('book_title',$data)){
            $this->book_title=$data['book_title'];
        }

        if(array_key_exists('author_title',$data)){
            $this->author_name=$data['author_title'];
        }

    }

    public function store(){
        $arrData=array($this->book_title, $this->author_name);
        $sql="INSERT INTO book_title (book_name,author_name) VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if ($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");

        Utility::redirect('create.php'); // redirect korte hobe create.php te tai utility.php use korechi //

    }//end of store method


}


//$objBooktitle = new Booktitle();

