<?php

namespace App\SummaryOfOrganization;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class SummaryOfOrganization extends DB{

    public $id="01";
    public $name="WHO";
    public $organization="World Health Organaization";

    public function __construct()
    {
        parent::__construct();
    }

/*
    public function index(){
        echo $this->id."<br>";
        echo $this->name."<br>";
        echo $this->organization."<br>";
    }
*/

    public function setData($data=NULL){

        if(array_key_exists('name',$data)){
            $this->name=$data['name'];
        }

        if(array_key_exists('organization',$data)){
            $this->organization=$data['organization'];
        }

    }

    public function store(){
        $arrData=array($this->name, $this->organization);
        $sql="INSERT INTO summary_of_organization (name,organization) VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if ($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");

        Utility::redirect('create.php'); // redirect korte hobe create.php te tai utility.php use korechi //

    }//end of store method
}


//$objSummaryOfOrganization = new SummaryOfOrganization();